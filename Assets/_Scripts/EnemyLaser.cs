﻿using UnityEngine;
using System.Collections;

public class EnemyLaser : MonoBehaviour {

    [SerializeField] float speed;
    Rigidbody2D rb;

    void Start() {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.up * -1 * speed;
    }

    void OnTriggerEnter2D (Collider2D other) {
        if ( other.tag == "Player"){
            Destroy(gameObject);
        }
    }
}
