﻿using UnityEngine;
using System.Collections;

public class EnemyAggressive : Enemy {

    [SerializeField] GameObject enemyLaser;
    [SerializeField] Transform enemyLaserPosition;
    [SerializeField] float fireRate;

    float isReadyForShoot;
    Rigidbody2D rb;

    void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.up * -1 * Speed;
        isReadyForShoot = 0.0f;
    }
	
	void Update () {
        if (Time.time > isReadyForShoot)
        {
            isReadyForShoot = Time.time + fireRate;
            Instantiate(enemyLaser, enemyLaserPosition.position, Quaternion.identity);
        }
    }
}
