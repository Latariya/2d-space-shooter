﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    [SerializeField] float speed;
    [SerializeField] float score;
    [SerializeField] int health;

    [SerializeField] GameObject explosion;
    [SerializeField] GameObject laserHit;

    [SerializeField] GameObject playerLaser;
    [SerializeField] Transform playerLaserPosition;
    [SerializeField] float fireRate;

    public float Speed { get { return speed; } }

    Rigidbody2D rb;
    float isReadyForShoot;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        isReadyForShoot = 0.0f;
    }

    void Update()
    {
        if (Time.time > isReadyForShoot)
        {
            isReadyForShoot = Time.time + fireRate;
            Instantiate(playerLaser, playerLaserPosition.position, Quaternion.identity);
        }
    }

    void FixedUpdate ()
    {
        float moveVertical = Input.GetAxis("Vertical") * Speed;
        float moveHorizontal = Input.GetAxis("Horizontal") * Speed;
        rb.velocity = new Vector2(moveHorizontal, moveVertical);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        switch (other.tag)
        {
            case "EnemyLaser":
                Instantiate(laserHit, transform.position, transform.rotation);

                health--;
                if (health <= 0)
                {
                    Instantiate(explosion, transform.position, transform.rotation);
                    Destroy(gameObject);
                }
                break;

            case "Asteroid":
                Instantiate(explosion, transform.position, transform.rotation);
                Destroy(gameObject);
                break;
        }
    }

}
