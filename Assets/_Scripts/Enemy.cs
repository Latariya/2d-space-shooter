﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    [SerializeField] float speed;
    [SerializeField] float score;
    [SerializeField] int health;

    [SerializeField] GameObject explosion;
    [SerializeField] GameObject laserHit;
    
    Rigidbody2D rb;

    public float Speed { get { return speed; } }

    void Start () {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.up * -1 * Speed;
    }

    void OnTriggerEnter2D (Collider2D other) {
        switch (other.tag) {
            case "PlayerLaser":
                Instantiate(laserHit, transform.position, transform.rotation);

                health--;
                if (health <= 0) {
                    Instantiate(explosion, transform.position, transform.rotation);
                    Destroy(gameObject);
                }
                break;

            case "Player":
                Instantiate(explosion, transform.position, transform.rotation);
                Destroy(gameObject);
                break;
        }
    }
}
