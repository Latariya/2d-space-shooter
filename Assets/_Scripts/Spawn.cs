﻿/* Script for spawning objects along X axis in given Y coordinates */

using UnityEngine;
using System.Collections;

[System.Serializable]
public class SpawnItem
{
    [SerializeField] GameObject objectForSpawn;
    [SerializeField] float count;
    [SerializeField] float objectSpawnTime;
    [SerializeField] float objectWaveSpawnTime;

    public float ObjectWaveSpawnTime { get { return objectWaveSpawnTime; } }
    public GameObject ObjectForSpawn { get { return objectForSpawn; } }
    public float ObjectSpawnTime { get { return objectSpawnTime; } }
    public float Count { get { return count; } }
}

public class Spawn : MonoBehaviour {

    [SerializeField] SpawnItem objectOne;
    [SerializeField] SpawnItem objectTwo;
    [SerializeField] SpawnItem objectThree;
    [SerializeField] SpawnItem objectFour;

    [SerializeField] float firstWaveSpawnTime;
    [SerializeField] Transform dataForSpawnPosition;
   
	void Start () {
        StartCoroutine(spawnThis(objectOne));
        StartCoroutine(spawnThis(objectTwo));
        StartCoroutine(spawnThis(objectThree));
        StartCoroutine(spawnThis(objectFour));
    }
    
    IEnumerator spawnThis (SpawnItem objectForSpawn)
    {
        yield return new WaitForSeconds(firstWaveSpawnTime);

        while (true)
        {
            for (int i = 0; i < objectForSpawn.Count; i++)
            {
                Vector2 thisObjectSpawnPosition = new Vector2(Random.Range(-5f, 5f), dataForSpawnPosition.position.y);
                Quaternion thisObjectRotation = Quaternion.identity;
                Instantiate(objectForSpawn.ObjectForSpawn, thisObjectSpawnPosition, thisObjectRotation);

                yield return new WaitForSeconds(objectForSpawn.ObjectSpawnTime);
            }
            yield return new WaitForSeconds(objectForSpawn.ObjectWaveSpawnTime);
        }
    }
}
